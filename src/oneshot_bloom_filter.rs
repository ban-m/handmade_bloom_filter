/// Implementation of "lower-bound" BF.
/// Here, "lower-bound" bloom filter means that it gives the lower bound estimation of
/// a given query.
/// This struct only support construct dataset with one-shot manner, in other words,
/// users can only insert elements while constructing a lower bound BF.
/// This is motivated by the need for parallelized insertion.
/// All of the operation are higly parallelized.
/// To construct a lower bound BF, one should construct by using factory pattern.
use super::BASES;
use super::DEFAULT_HASH_NUM;
use super::DEFAULT_MODULO;
use rayon::prelude::*;
#[derive(Debug)]
struct CountVec {
    inner: Vec<u16>,
    occupied: usize,
}

impl std::ops::Index<usize> for CountVec {
    type Output = u16;
    fn index(&self, index: usize) -> &u16 {
        &self.inner[index]
    }
}

impl CountVec {
    fn len(&self) -> usize {
        self.inner.len()
    }
    fn occupied(&self) -> usize {
        self.inner.iter().filter(|&&e| e != 0).count()
    }
    fn ratio(&self) -> f64 {
        self.occupied() as f64 / self.inner.len() as f64
    }
    fn new(len: usize) -> Self {
        Self {
            inner: vec![0; len],
            occupied: 0,
        }
    }
    fn add(&mut self, index: usize) {
        if self.inner[index] != 255 {
            self.inner[index] += 1;
        }
    }
    fn add_by(&mut self, index: usize, count: u16) {
        if self.inner[index] != 255 {
            self.inner[index] = self.inner[index].saturating_add(count);
        }
    }
}

#[derive(Debug)]
pub struct UpperBoundBF<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    cv: CountVec,
    modulo: usize,
    number_of_hash: usize,
    k: usize,
    mapper1: F,
    mapper2: E,
    modulo_4: usize, // 4 ** k % modulo,
}

impl<F, E> std::fmt::Display for UpperBoundBF<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "Upper Bound BloomFilter for {}-mer", self.k)?;
        writeln!(f, "Mapper1")?;
        for base in &BASES {
            writeln!(f, "{}->{:02b}", *base as char, (self.mapper1)(*base))?;
        }
        writeln!(f, "Mapper2")?;
        for base in &BASES {
            writeln!(f, "{}->{:02b}", *base as char, (self.mapper2)(*base))?;
        }
        writeln!(f, "Modulo:{}(prime)", self.modulo)?;
        writeln!(f, "Number of hash:{}", self.number_of_hash)?;
        if self.modulo < 1_000 {
            writeln!(f, "CV:{:?}", self.cv)?;
        }
        let ratio = self.cv.ratio();
        writeln!(
            f,
            "Occupied/Empty={}/{}={}",
            self.cv.occupied(),
            self.cv.len(),
            ratio,
        )?;
        let error_rate = ratio.powi(self.number_of_hash as i32);
        writeln!(f, "Error Rate:{}",error_rate)?;
        let num_of_uniq = (1. / (1. - ratio)).ln() * (self.modulo / self.number_of_hash) as f64;
        writeln!(f, "Estimated # of unique elements:{}", num_of_uniq,)?;
        let num_of_hash = (self.modulo as f64 / num_of_uniq * 2f64.ln()).floor() as u32;
        write!(f, "Recomended # of hash function:{}", num_of_hash)?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct UpperBoundBFFactory<'a, E, F>
where
    E: Fn(u8) -> u8,
    F: Fn(u8) -> u8,
{
    modulo: usize,
    number_of_hash: usize,
    k: usize,
    mapper1: E,
    mapper2: F,
    dataset: &'a [Vec<u8>],
}

impl<'a, E, F> UpperBoundBFFactory<'a, E, F>
where
    E: Fn(u8) -> u8,
    F: Fn(u8) -> u8,
{
    pub fn new(
        mapper1: E,
        mapper2: F,
        modulo: usize,
        number_of_hash: usize,
        k: usize,
        dataset: &'a [Vec<u8>],
    ) -> UpperBoundBFFactory<E, F> {
        UpperBoundBFFactory {
            mapper1,
            mapper2,
            modulo,
            number_of_hash,
            k,
            dataset,
        }
    }
    pub fn mapper1<G>(self, m1: G) -> UpperBoundBFFactory<'a, G, F>
    where
        G: Fn(u8) -> u8,
    {
        UpperBoundBFFactory::new(
            m1,
            self.mapper2,
            self.modulo,
            self.number_of_hash,
            self.k,
            self.dataset,
        )
    }
    pub fn mapper2<G>(self, m2: G) -> UpperBoundBFFactory<'a, E, G>
    where
        G: Fn(u8) -> u8,
    {
        UpperBoundBFFactory::new(
            self.mapper1,
            m2,
            self.modulo,
            self.number_of_hash,
            self.k,
            self.dataset,
        )
    }
    pub fn modulo(self, modulo: usize) -> Self {
        Self { modulo, ..self }
    }
    pub fn k(self, k: usize) -> Self {
        Self { k, ..self }
    }
    pub fn number_of_hash(self, number_of_hash: usize) -> Self {
        Self {
            number_of_hash,
            ..self
        }
    }
    pub fn add_dataset<'b>(self, dataset: &'b [Vec<u8>]) -> UpperBoundBFFactory<'b, E, F> {
        UpperBoundBFFactory::new(
            self.mapper1,
            self.mapper2,
            self.modulo,
            self.number_of_hash,
            self.k,
            dataset,
        )
    }
    pub fn finalize(self) -> UpperBoundBF<E, F> {
        let modulo_4 = modulo_k_2(2 * self.k, self.modulo);
        let mut bf = UpperBoundBF {
            cv: CountVec::new(self.modulo),
            modulo: self.modulo,
            number_of_hash: self.number_of_hash,
            k: self.k,
            mapper1: self.mapper1,
            mapper2: self.mapper2,
            modulo_4: modulo_4,
        };
        bf.insert_strings(self.dataset);
        bf
    }
}
impl<'a, E, F> UpperBoundBFFactory<'a, E, F>
where
    E: Fn(u8) -> u8 + Send + Sync,
    F: Fn(u8) -> u8 + Send + Sync,
{
    pub fn finalize_par(self, t: usize) -> UpperBoundBF<E, F> {
        if t == 0 {
            return self.finalize();
        }
        let modulo_4 = modulo_k_2(2 * self.k, self.modulo);
        let memory = 5_000_000_000; //10Gb
        let ave = self.dataset.iter().map(|e| e.len()).sum::<usize>() / self.dataset.len();
        let chunk_size = memory / ave / self.number_of_hash;
        let mut bf = UpperBoundBF {
            cv: CountVec::new(self.modulo),
            modulo: self.modulo,
            number_of_hash: self.number_of_hash,
            k: self.k,
            mapper1: self.mapper1,
            mapper2: self.mapper2,
            modulo_4: modulo_4,
        };
        for (_i, chunk) in self.dataset.chunks(chunk_size).enumerate() {
            bf.insert_strings_par(t, chunk).unwrap();
        }
        bf
    }
}

#[inline]
fn mapper1(x: u8) -> u8 {
    match x {
        b'A' | b'a' => 0,
        b'C' | b'c' => 1,
        b'G' | b'g' => 2,
        b'T' | b't' => 3,
        _ => 0,
    }
}

#[inline]
fn mapper2(x: u8) -> u8 {
    match x {
        b'A' | b'a' => 3,
        b'C' | b'c' => 1,
        b'G' | b'g' => 0,
        b'T' | b't' => 2,
        _ => 0,
    }
}

fn is_prime(x: usize) -> bool {
    let mut y = 2;
    while y * y <= x {
        if x % y == 0 {
            return false;
        }
        y += 1;
    }
    true
}

const EMPTY: [Vec<u8>; 0] = [];

type Mapper = fn(u8) -> u8;
impl UpperBoundBFFactory<'static, Mapper, Mapper> {
    pub fn default() -> Self {
        UpperBoundBFFactory {
            modulo: DEFAULT_MODULO,
            number_of_hash: DEFAULT_HASH_NUM,
            k: 0,
            mapper1: mapper1,
            mapper2: mapper2,
            dataset: &EMPTY,
        }
    }
}

// Compute 2 ** t % modulo
#[inline]
fn modulo_k_2(t: usize, modulo: usize) -> usize {
    if t < 30 {
        return 2usize.pow(t as u32) % modulo;
    }
    let child = modulo_k_2(t / 2, modulo);
    if t % 2 == 0 {
        modulo_mul(child, child, modulo)
    } else {
        (modulo_mul(child, child, modulo) * 2) % modulo
    }
}

impl UpperBoundBF<Mapper, Mapper> {
    pub fn new_with_default(k: usize) -> Self {
        // Some prime
        debug_assert!(is_prime(DEFAULT_MODULO));
        UpperBoundBFFactory::default().k(k).finalize()
    }
    // Return a Bloom Filter with given configuration. None if the modulo is, in fact,
    // not a prime number.
    pub fn new_with_configures(modulo: usize, hashernum: usize, k: usize) -> Option<Self> {
        match is_prime(modulo) {
            true => Some(
                UpperBoundBFFactory::default()
                    .k(k)
                    .modulo(modulo)
                    .number_of_hash(hashernum)
                    .finalize(),
            ),
            false => None,
        }
    }
}

impl<F, E> UpperBoundBF<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    /// Return a Bloom Filter with given configuration. None if the modulo is, in fact,
    /// not a prime number.
    /// In this configuration, is is responsible of user to carefully design the map function so that the performance would not be colluplsed. For example,
    /// giving the same mapper would be harmful.
    pub fn new_with_full_configures(
        modulo: usize,
        hashernum: usize,
        k: usize,
        m1: F,
        m2: E,
    ) -> Option<Self> {
        match is_prime(modulo) {
            true => Some(
                UpperBoundBFFactory::default()
                    .k(k)
                    .modulo(modulo)
                    .mapper1(m1)
                    .mapper2(m2)
                    .number_of_hash(hashernum)
                    .finalize(),
            ),
            false => None,
        }
    }
}

// (h * mul) % modulo
#[inline]
fn modulo_mul(h: usize, mul: usize, modulo: usize) -> usize {
    // let h = h % modulo;
    // let mul = mul % modulo;
    let mut y = 0;
    for _ in 0..mul {
        y += h;
        if y > modulo {
            y -= modulo;
        }
    }
    y
}

impl<F, E> UpperBoundBF<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    pub fn occupied_bits(&self) -> usize {
        self.cv.occupied()
    }
    pub fn modulo(&self) -> usize {
        self.modulo
    }
    pub fn ratio(&self) -> f64 {
        self.cv.ratio()
    }
    #[inline]
    fn hash_value_inner<T>(&self, mapper: T, kmer: &[u8]) -> usize
    where
        T: Fn(u8) -> u8,
    {
        let mut res = 0usize;
        for &base in kmer {
            let bits = mapper(base) as usize;
            res = res << 2;
            res |= bits;
            res %= self.modulo;
        }
        res
    }
    // The hash values should be computed 'reverse' manner, i.e.,
    // the last character is to be regarded as
    // the least siginificant bit.
    #[inline]
    fn hash_value(&self, kmer: &[u8]) -> (usize, usize) {
        (
            self.hash_value_inner(&self.mapper1, kmer),
            self.hash_value_inner(&self.mapper2, kmer),
        )
    }
    #[inline]
    fn fill_hash_values(&mut self, h1: usize, h2: usize) {
        let mut hv = h1;
        self.cv.add(hv);
        for _ in 1..self.number_of_hash {
            hv = hv + h2;
            if hv >= self.modulo {
                hv -= self.modulo;
            }
            self.cv.add(hv);
        }
    }
    /// Return Some(true) if the input kmer is novel kmer,
    /// Return Some(false) if the input is already in, and
    /// Return None if the input is too short.
    #[inline]
    pub fn insert(&mut self, kmer: &[u8]) -> Option<()> {
        if kmer.len() != self.k {
            None
        } else {
            Some(self.insert_string_inner(kmer))
        }
    }
    // 4 * h + m - (4**k)*l mod M
    #[inline]
    fn next_with_underflow(&self, h: usize, m: usize, l: usize) -> usize {
        let subset = modulo_mul(self.modulo_4, l, self.modulo);
        let whole = (h << 2) + m;
        if subset > whole {
            (self.modulo + whole) - subset
        } else {
            (whole - subset) % self.modulo
        }
    }
    #[inline]
    fn next_hash_value(&self, h1: usize, h2: usize, x: u8, y: u8) -> (usize, usize) {
        let (m1, m2) = ((self.mapper1)(x) as usize, (self.mapper2)(x) as usize);
        let (l1, l2) = ((self.mapper1)(y) as usize, (self.mapper2)(y) as usize);
        (
            self.next_with_underflow(h1, m1, l1),
            self.next_with_underflow(h2, m2, l2),
        )
    }
    fn insert_string_inner(&mut self, input: &[u8]) {
        let (mut h1, mut h2) = self.hash_value(&input[0..self.k]);
        for i in self.k..input.len() {
            self.fill_hash_values(h1, h2);
            let (n1, n2) = self.next_hash_value(h1, h2, input[i], input[i - self.k]);
            h1 = n1;
            h2 = n2;
        }
        self.fill_hash_values(h1, h2);
    }
    #[inline]
    pub fn insert_string(&mut self, input: &[u8]) -> Option<()> {
        if input.len() < self.k {
            None
        } else {
            Some(self.insert_string_inner(input))
        }
    }
    #[inline]
    fn insert_strings_inner(&mut self, input: &[Vec<u8>], k: usize) {
        for line in input.iter().filter(|line| line.len() >= k) {
            self.insert_string_inner(line);
        }
    }
    pub fn insert_strings(&mut self, input: &[Vec<u8>]) -> Option<()> {
        let k = self.k;
        if input.iter().all(|line| line.len() < k) {
            None
        } else {
            Some(self.insert_strings_inner(input, k))
        }
    }
    #[inline]
    fn fill_bits(&mut self, hvs: &Vec<usize>) {
        let (mut current, mut count) = (hvs[0], 1);
        for &pos in &hvs[1..] {
            if pos != current {
                self.cv.add_by(current, count);
                current = pos;
                count = 1;
            } else if count != 0xFFFF {
                count += 1;
            }
        }
        self.cv.add_by(current, count);
    }
    #[inline]
    fn ub_inner(&self, h1: usize, h2: usize) -> u16 {
        let mut res = vec![];
        let mut hv = h1;
        res.push(hv);
        let mut min = self.cv[hv];
        for _ in 1..self.number_of_hash {
            hv = hv + h2;
            if hv >= self.modulo {
                hv -= self.modulo;
            }
            min = min.min(self.cv[hv]);
            res.push(hv);
        }
        res.sort();
        min
    }
    #[inline]
    pub fn upper_bound(&self, kmer: &[u8]) -> Option<u16> {
        if kmer.len() != self.k {
            None
        } else {
            let (h1, h2) = self.hash_value(kmer);
            Some(self.ub_inner(h1, h2))
        }
    }
    #[inline]
    pub fn upper_bound_at_each_position(&self, line: &[u8]) -> Vec<u16> {
        let mut pos = vec![];
        if line.len() >= self.k {
            let (mut h1, mut h2) = self.hash_value(&line[0..self.k]);
            for i in self.k..line.len() {
                pos.push(self.ub_inner(h1, h2));
                let (n1, n2) = self.next_hash_value(h1, h2, line[i], line[i - self.k]);
                h1 = n1;
                h2 = n2;
            }
            pos.push(self.ub_inner(h1, h2));
        }
        pos
    }
    #[inline]
    pub fn position_of_more_than(&self, line: &[u8], lower: u16) -> Vec<usize> {
        let mut pos = vec![];
        if line.len() >= self.k {
            let (mut h1, mut h2) = self.hash_value(&line[0..self.k]);
            for i in self.k..line.len() {
                if self.ub_inner(h1, h2) >= lower {
                    pos.push(i - self.k);
                }
                let (n1, n2) = self.next_hash_value(h1, h2, line[i], line[i - self.k]);
                h1 = n1;
                h2 = n2;
            }
            if self.ub_inner(h1, h2) >= lower {
                pos.push(line.len() - self.k);
            };
        }
        pos
    }
}

impl<F, E> UpperBoundBF<F, E>
where
    F: Fn(u8) -> u8 + Send + Sync,
    E: Fn(u8) -> u8 + Send + Sync,
{
    pub fn insert_strings_par(&mut self, t: usize, input: &[Vec<u8>]) -> Option<()> {
        if t == 1 {
            self.insert_strings(input)
        } else {
            // Reserve one thread to reducing.
            let hvs = self.bits_to_be_filled(t - 1, input);
            self.fill_bits(&hvs);
            Some(())
        }
    }
    fn send_bits_to_be_filled(&self, h1: usize, h2: usize, bits: &mut Vec<usize>) {
        let mut hv = h1;
        bits.push(hv);
        for _ in 1..self.number_of_hash {
            hv += h2;
            if hv >= self.modulo {
                hv -= self.modulo;
            }
            bits.push(hv);
        }
    }
    fn send_all_bits_to_be_filled(&self, input: &Vec<u8>, bits: &mut Vec<usize>) {
        let (mut h1, mut h2) = self.hash_value(&input[0..self.k]);
        for i in self.k..input.len() {
            self.send_bits_to_be_filled(h1, h2, bits);
            let (n1, n2) = self.next_hash_value(h1, h2, input[i], input[i - self.k]);
            h1 = n1;
            h2 = n2;
        }
        self.send_bits_to_be_filled(h1, h2, bits);
    }
    fn bits_to_be_filled(&self, t: usize, input: &[Vec<u8>]) -> Vec<usize> {
        let len = input.len();
        let chunksize = len / t;
        crossbeam::scope(|s| {
            let mut res = Vec::with_capacity(0);
            let (tx, rx) = crossbeam::channel::unbounded();
            for i in 0..t {
                let start = i * chunksize;
                let end = if i != t - 1 { (i + 1) * chunksize } else { len };
                let tx = tx.clone();
                s.spawn(move |_| {
                    let mut bits = vec![];
                    for j in start..end {
                        let line = &input[j];
                        if line.len() >= self.k {
                            self.send_all_bits_to_be_filled(line, &mut bits);
                        }
                    }
                    tx.send(bits).unwrap();
                });
            }
            drop(tx);
            while let Ok(loc) = rx.recv() {
                res.extend(loc);
            }
            res.par_sort_unstable();
            res
        })
        .unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    fn mock_mapper(_: u8) -> u8 {
        0
    }
    #[test]
    fn initialize_test() {
        let _bf = UpperBoundBF::new_with_default(12);
        assert!(UpperBoundBF::new_with_configures(173, 6, 12).is_some());
        assert!(
            UpperBoundBF::new_with_full_configures(173, 6, 12, mock_mapper, mock_mapper).is_some()
        );
        assert!(UpperBoundBF::new_with_configures(171, 6, 12).is_none());
        assert!(
            UpperBoundBF::new_with_full_configures(171, 6, 12, mock_mapper, mock_mapper).is_none()
        );
    }
    #[test]
    fn insert_test() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmer = b"CATCATCGTAGCTT";
        assert_eq!(bf.insert(kmer), Some(()));
        let kmer = b"AT";
        assert_eq!(bf.insert(kmer), None);
    }
    #[test]
    fn insert_string_test() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmer = b"ATTTACATCAGTGTCAGTCATGTCAGTGTCAT";
        assert_eq!(bf.insert_string(kmer), Some(()));
        assert_eq!(bf.insert_string(kmer), Some(()));
        let kmer = b"ATTTACATCAGTGTCAGTCATGTCAGTGTGAT";
        assert_eq!(bf.insert_string(kmer), Some(()));
        let kmer = b"ATTTA";
        assert_eq!(bf.insert_string(kmer), None);
    }
    #[test]
    fn insert_string_test2() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmer = b"CATGTCAGTGTCAT";
        assert_eq!(bf.insert_string(kmer), Some(()));
        assert_eq!(bf.insert_string(kmer), Some(()));
        let kmer = b"CATGTCAGTGTGAT";
        assert_eq!(bf.insert_string(kmer), Some(()));
        let kmer = b"ATTTA";
        assert_eq!(bf.insert_string(kmer), None);
    }

    #[test]
    fn insert_strings_test() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
        ];
        assert_eq!(bf.insert_strings(&kmers), Some(()));
        assert_eq!(bf.insert_strings(&kmers), Some(()));
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        assert_eq!(bf.insert_strings(&kmers), Some(()));
    }
    #[test]
    fn retain_test() {
        let mut bf = UpperBoundBF::new_with_default(4);
        let kmer = b"AATG";
        bf.insert(kmer);
        assert_eq!(bf.upper_bound(kmer), Some(1));
        let kmer = b"AATC";
        assert_eq!(bf.upper_bound(kmer), Some(0));
    }
    #[test]
    fn retain_string_test() {
        let mut bf = UpperBoundBF::new_with_default(4);
        let line = b"AATGC";
        bf.insert_string(line);
        bf.insert_string(&line[..4]);
        bf.insert_string(&line[1..]);
        for kmer in line.windows(4) {
            assert_eq!(bf.upper_bound(kmer), Some(2));
        }
    }
    #[test]
    fn multiple_insertion_test() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmer = b"CATCATCGTAGCTT";
        assert_eq!(bf.insert(kmer), Some(()));
        assert_eq!(bf.insert(kmer), Some(()));
        let kmer = b"ATTTACATCAGTGTCAGTCATGTCAGTGTCAT";
        assert_eq!(bf.insert(kmer), None);
        let kmer = b"ATTT";
        assert_eq!(bf.insert(kmer), None);
    }
    #[test]
    fn multiple_retain_test() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmer = b"CATCATCGTAGCTT";
        assert_eq!(bf.insert(kmer), Some(()));
        assert_eq!(bf.insert(kmer), Some(()));
        let kmer2 = b"CATCATCGTAGCTG";
        assert_eq!(bf.insert(kmer2), Some(()));
        assert_eq!(bf.upper_bound(kmer), Some(2));
        assert_eq!(bf.upper_bound(kmer2), Some(1));
    }
    #[test]
    fn retain_test_2_normal() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let line = b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec();
        assert_eq!(bf.insert_string(&line), Some(()));
        for kmer in line.windows(14) {
            assert_eq!(bf.upper_bound(kmer), Some(1));
        }
    }
    #[test]
    fn multiple_retain_test_2() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        for line in &kmers {
            assert_eq!(bf.insert_string(line), Some(()));
            assert_eq!(bf.insert_string(line), Some(()));
        }
        for line in &kmers {
            for kmer in line.windows(14) {
                assert!(bf.upper_bound(kmer).unwrap() >= 2);
            }
        }
    }
    #[test]
    fn multiple_retain_test_par() {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        bf.insert_strings_par(1, &kmers).unwrap();
        bf.insert_strings_par(1, &kmers).unwrap();
        for line in &kmers {
            for kmer in line.windows(14) {
                assert!(bf.upper_bound(kmer).unwrap() >= 2);
            }
        }
    }
    #[test]
    fn multiple_retain_test_par2() {
        let line = vec![b"AATCGATGCT".to_vec()];
        let bf = UpperBoundBFFactory::default()
            .k(10)
            .add_dataset(&line)
            .finalize_par(2);
        let res = dbg!(bf.upper_bound(&line[0]));
        assert!(res.unwrap() >= 1);
    }
    #[test]
    fn retain_test_par() {
        let (length, num) = (50, 1);
        let mut lines = gen_data(length, num);
        lines.extend(lines.clone());
        let mut bf = UpperBoundBFFactory::default().k(10).finalize();
        bf.insert_strings_par(5, &lines);
        for line in &lines {
            let res = dbg!(bf.upper_bound_at_each_position(line));
            assert!(res.into_iter().all(|e| e >= 2));
        }
    }

    use test::Bencher;
    #[bench]
    fn lb_speed(b: &mut Bencher) {
        let mut bf = UpperBoundBF::new_with_default(14);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        bf.insert_strings(&kmers);
        let kmers: Vec<Vec<u8>> = kmers
            .into_iter()
            .flat_map(|e| e.windows(14).map(|kmer| kmer.to_vec()).collect::<Vec<_>>())
            .collect();
        b.iter(|| {
            for kmer in &kmers {
                bf.upper_bound(kmer);
            }
        });
    }
    #[bench]
    fn insert_speed(b: &mut Bencher) {
        let mut bf = UpperBoundBF::new_with_default(20);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CATTGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        let hash_values: Vec<(usize, usize)> = kmers
            .into_iter()
            .flat_map(|e| {
                e.windows(20)
                    .map(|kmer| bf.hash_value(kmer))
                    .collect::<Vec<_>>()
            })
            .collect();
        b.iter(|| {
            for &(h1, h2) in &hash_values {
                bf.fill_hash_values(h1, h2);
            }
        });
    }
    #[bench]
    fn hash_value_speed(b: &mut Bencher) {
        let bf = UpperBoundBF::new_with_default(20);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CATGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        let mut sum = 0;
        b.iter(|| {
            for input in &kmers {
                let (mut h1, mut h2) = bf.hash_value(&input[0..20]);
                for i in 20..input.len() {
                    let (n1, n2) = bf.next_hash_value(h1, h2, input[i], input[i - 20]);
                    h1 = n1;
                    h2 = n2;
                }
                sum += h1;
                sum += h2;
            }
        });
    }
    use rand::{seq::SliceRandom, thread_rng, Rng};
    #[bench]
    fn insert_string_speed_length10_000(b: &mut Bencher) {
        let mut bf = UpperBoundBF::new_with_default(20);
        let mut rng = thread_rng();
        let line: Vec<_> = (0..10_000)
            .filter_map(|_| BASES.choose(&mut rng))
            .map(|&e| e)
            .collect();
        b.iter(|| bf.insert_string(&line).unwrap());
    }
    #[bench]
    fn fill_hash_value_speed_length_10_000(b: &mut Bencher) {
        let mut bf = UpperBoundBF::new_with_default(20);
        let mut rng = thread_rng();
        let line: Vec<_> = (0..10_000)
            .filter_map(|_| BASES.choose(&mut rng))
            .map(|&e| e)
            .collect();
        let hash_values: Vec<(usize, usize)> =
            line.windows(20).map(|kmer| bf.hash_value(kmer)).collect();
        b.iter(|| {
            for &(h1, h2) in &hash_values {
                bf.fill_hash_values(h1, h2)
            }
        });
    }
    #[bench]
    fn add_speed_10_000_20(b: &mut Bencher) {
        let mut cv = CountVec::new(DEFAULT_MODULO);
        let mut rng = thread_rng();
        let num = (10_000 - 20 + 1) * DEFAULT_HASH_NUM; // # of kmer * # of hash
        let add: Vec<_> = (0..num).map(|_| rng.gen_range(0, DEFAULT_MODULO)).collect();
        b.iter(|| {
            for &x in &add {
                cv.add(x);
            }
        })
    }
    fn gen_data(len: usize, num: usize) -> Vec<Vec<u8>> {
        let mut rng = thread_rng();
        (0..num)
            .map(|_| {
                (0..len)
                    .filter_map(|_| BASES.choose(&mut rng))
                    .map(|&e| e)
                    .collect()
            })
            .collect()
    }

    // #[bench]
    // fn insert_strings_100_000x2_000(b: &mut Bencher) {
    //     let mut bf = UpperBoundBF::new_with_default(20);
    //     let (length, num) = (2_000, 100_000);
    //     let line = gen_data(length,num);
    //     b.iter(|| bf.insert_strings(&line));
    // }
    // #[bench]
    // fn insert_strings_par_100_000x2_000(b: &mut Bencher) {
    //     let mut bf = UpperBoundBF::new_with_default(20);
    //     let (length, num) = (2_000, 100_000);
    //     let t = 10;
    //     let line = gen_data(length,num);
    //     let t = 10;
    //     b.iter(|| bf.insert_strings_par(t, &line));
    // }
    // #[bench]
    // fn bits_to_be_filled_10_000x2_000_par(b: &mut Bencher) {
    //     let bf = UpperBoundBF::new_with_default(20);
    //     let t = 10;
    //     let (length, num) = (2_000, 100_000);
    //     let line = gen_data(length,num);
    //     b.iter(|| bf.bits_to_be_filled(t, &line));
    // }
    #[bench]
    fn fill_bits_1_000x2_000(b: &mut Bencher) {
        let mut bf = UpperBoundBF::new_with_default(20);
        let (length, num) = (1_000, 2_000);
        let line = gen_data(length, num);
        let mut hvs = bf.bits_to_be_filled(1, &line);
        hvs.sort();
        b.iter(|| bf.fill_bits(&hvs));
    }
}
