#![feature(test)]
//! A tiny and lightweight implementation of [Bloom Filter](https://dl.acm.org/citation.cfm?doid=362686.362692)p
//! to replesent k-mer/k-gram of a given input.
//! It is designed to process **DNA seuqence without any ambiguous character**. Thus, make sure each
//! input is a string on the alphabet:\Sigma = {A,T,G,C,a,t,g,c}(lowercase lettes would automatically converted into uppercase letter).
//! input you push is
//! # How to use
//! To use
//!To use this implementation in your program, clone this repository to `/directory/you/want`,
//!then ,add
//!```toml
//![dependencies]
//!handmade_bloom_filter = {path = "/direcory/you/want/handmade_bloom_filter/"}
//!```
//!to your `Cargo.toml`. Hereafter,
//!```ignore
//!extern crate handmede_bloom_filter;
//!fn main(){
//!let bf = handmade_bloom_filter::BloomFilter::new();
//!}
//!```
//!would be compiled.
//! Currently, you can not merge two Bloom Filters, I can not determine if the two Bloom filters are `equivalent` with respect to two hashers. In other words, it is indeed needed to merge two Bloom Filter not only that the filters has the same Modulo, but also that they do have the same hashers.
//! Sadly, comparing two closure(hasher) is not easy task at all in general. Maybe I should re-implement the structure to explicitly parametlize the hasher set. For example, when I create
//! hasher set `hs`, a Bloom Filter can be created with reference to that hashset(&hs).
//! In this definition, the equivalence with respect to hasher set can be more strictly defined as `having the same &hs`.

///
/// Implementation of BloomFilter. It has two main compomens: bit-vector and two hash functions.
/// The bitvector is to represent bit-strings to `remember` a given signature of k-mer/k-gram,
/// while the two mapping function, along with its linear combinations, a given k-mer into its signature.
/// To calculate the signature, classical rolling-hashing would be applied after convert DNA strings into u8 integers
/// by each functions.
/// The length of the signature, the length of bit vector, and the two convert functions can be configured at initialization step.
/// Otherwise, the default setting would be used.
///
/// When you use this struct in default mode rather than in configured settings,
/// I hightly recommend to input sequence into this bloom filter by `one-shot`. In other words, calling `with_insert_strings`
/// on **all** inputs, rather than call `insert_string` on **each** string separately after initialization.
/// This is because, in the former settings, the struct first detects the length of the input, and determin the length of bitvector,
/// and estimate its unique kmer and number of linear combinations of hash functions.
extern crate test;
extern crate rayon;
extern crate rand;
extern crate crossbeam;
mod bitvector;
pub mod oneshot_bloom_filter;
pub use oneshot_bloom_filter::{UpperBoundBFFactory,UpperBoundBF};
use self::bitvector::BitVector;
const BASES: [u8; 4] = [b'A', b'C', b'G', b'T'];
const DEFAULT_MODULO: usize = 15_487_019;
pub const BIG_MODULO:usize = 2_038_081_147;
pub const HUGE_MODULO:usize = 13_359_557_801; 
const DEFAULT_HASH_NUM: usize = 12;

pub struct BloomFilter<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    // Should be longer than `length`
    bv: BitVector,
    modulo: usize,
    number_of_hash: usize,
    k: usize,
    mapper1: F,
    mapper2: E,
    mask: usize,
    modulo_4: usize, // 4 ** k % modulo
}

impl<F, E> std::fmt::Display for BloomFilter<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "BloomFilter for {}-mer", self.k)?;
        writeln!(f, "Mapper1")?;
        for base in &BASES {
            writeln!(f, "{}->{:02b}", *base as char, (self.mapper1)(*base))?;
        }
        writeln!(f, "Mapper2")?;
        for base in &BASES {
            writeln!(f, "{}->{:02b}", *base as char, (self.mapper2)(*base))?;
        }
        write!(f, "{}", self.bv)?;
        writeln!(f, "Modulo:{}(prime)", self.modulo)?;
        writeln!(f, "Number of hash:{}", self.number_of_hash)?;
        Ok(())
    }
}

// Fill last 2k bits with 1. For example, if k = 2 then return 0b000...001111.
// Since the length of usize does depend on the arch,
// I employ brute force approach here.
fn create_masking_bits(k: usize) -> usize {
    let mut res = 0;
    for i in 0..2 * k {
        res |= 1 << i;
        if res == usize::max_value(){
            return usize::max_value()
        }
    }
    res
}

// Compute 2 ** t % modulo
#[inline]
fn modulo_k_2(t: usize, modulo: usize) -> usize {
    if t < 30 {
        return 2usize.pow(t as u32) % modulo;
    }
    let child = modulo_k_2(t / 2, modulo);
    if t % 2 == 0 {
        modulo_mul(child, child, modulo)
    } else {
        (modulo_mul(child, child, modulo) * 2) % modulo
    }
}


#[inline]
fn mapper1(x: u8) -> u8 {
    match x {
        b'A' | b'a' => 0,
        b'C' | b'c' => 1,
        b'G' | b'g' => 2,
        b'T' | b't' => 3,
        _ => 0,
    }
}

#[inline]
fn mapper2(x: u8) -> u8 {
    match x {
        b'A' | b'a' => 3,
        b'C' | b'c' => 1,
        b'G' | b'g' => 0,
        b'T' | b't' => 2,
        _ => 0,
    }
}

fn is_prime(x: usize) -> bool {
    let mut y = 2;
    while y * y <= x {
        if x % y == 0 {
            return false;
        }
        y += 1;
    }
    true
}

impl BloomFilter<fn(u8) -> u8, fn(u8) -> u8> {
    pub fn new_with_default(k: usize) -> BloomFilter<fn(u8) -> u8, fn(u8) -> u8> {
        // Some prime
        debug_assert!(is_prime(DEFAULT_MODULO));
        BloomFilter {
            mapper1: mapper1,
            mapper2: mapper2,
            bv: BitVector::new(),
            modulo: DEFAULT_MODULO,
            number_of_hash: DEFAULT_HASH_NUM,
            k: k,
            mask: create_masking_bits(k),
            modulo_4: modulo_k_2(2 * k, DEFAULT_MODULO),
        }
    }
    // Return a Bloom Filter with given configuraon. None if the modulo is, in fact,
    // not a prime number.
    pub fn new_with_configures(
        modulo: usize,
        num_of_hasher: usize,
        k: usize,
    ) -> Option<BloomFilter<fn(u8) -> u8, fn(u8) -> u8>> {
        if is_prime(modulo) {
            Some(BloomFilter {
                mapper1: mapper1,
                mapper2: mapper2,
                bv: BitVector::new(),
                modulo: modulo,
                number_of_hash: num_of_hasher,
                k: k,
                mask: create_masking_bits(k),
                modulo_4: modulo_k_2(2 * k, modulo),
            })
        } else {
            None
        }
    }
    pub fn new_with_dataset(
        modulo: usize,
        k: usize,
        data: &[Vec<u8>],
    ) -> Option<BloomFilter<fn(u8) -> u8, fn(u8) -> u8>> {
        if is_prime(modulo) {
            let predicted_hashnumber = predict_hash_number(data, modulo, k);
            let mut bf = Self::new_with_configures(modulo, predicted_hashnumber, k)?;
            bf.insert_strings(data)?;
            Some(bf)
        } else {
            None
        }
    }
}

fn predict_hash_number(data: &[Vec<u8>], modulo: usize, k: usize) -> usize {
    // Optimal number of hashfunction = modulo / number of distinct kmer  * log_2
    let distinct_kmer = count_distinct_kmer(data, k);
    (std::f64::consts::LN_2 * (modulo / distinct_kmer) as f64).floor() as usize
}

fn count_distinct_kmer(data: &[Vec<u8>], k: usize) -> usize {
    // Of course, we can not.
    (data.iter().map(|e| e.len() / k).sum::<usize>() / 30).max(200)
}

impl<F, E> BloomFilter<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    /// Return a Bloom Filter with given configuration. None if the modulo is, in fact,
    /// not a prime number.
    /// In this configuration, is is responsible of user to carefully design the map function so that the performance would not be colluplsed. For example,
    /// giving the same mapper would be harmful.
    pub fn new_with_full_configures(
        modulo: usize,
        num_of_hasher: usize,
        k: usize,
        mapper1: F,
        mapper2: E,
    ) -> Option<Self> {
        if is_prime(modulo) {
            let bv = BitVector::new();
            let bf = BloomFilter {
                mapper1: mapper1,
                mapper2: mapper2,
                bv: bv,
                modulo: modulo,
                number_of_hash: num_of_hasher,
                k: k,
                mask: create_masking_bits(k),
                modulo_4: modulo_k_2(2 * k, modulo),
            };
            Some(bf)
        } else {
            None
        }
    }
}

// (h * mul) % modulo
#[inline]
fn modulo_mul(h: usize, mul: usize, modulo: usize) -> usize {
    let h = h % modulo;
    let mul = mul % modulo;
    let mut y = 0;
    for _ in 0..mul{
        y += h;
        if y > modulo {
            y -= modulo;
        }
    }
    y
}

impl<F, E> BloomFilter<F, E>
where
    F: Fn(u8) -> u8,
    E: Fn(u8) -> u8,
{
    pub fn occupied_bits(&self) -> u64 {
        self.bv.count_ones()
    }
    
    #[inline]
    fn hash_value_inner<T>(&self, mapper: T, kmer: &[u8]) -> usize
    where
        T: Fn(u8) -> u8,
    {
        let mut res = 0usize;
        for &base in kmer {
            let bits = mapper(base) as usize;
            res = res << 2;
            res |= bits;
            res %= self.modulo;
        }
        res
    }
    // The hash values should be computed 'reverse' manner, i.e.,
    // the last character is to be regarded as
    // the least siginificant bit.
    #[inline]
    fn hash_value(&self, kmer: &[u8]) -> (usize, usize) {
        (
            self.hash_value_inner(&self.mapper1, kmer),
            self.hash_value_inner(&self.mapper2, kmer),
        )
    }
    #[inline]
    fn fill_hash_values(&mut self, h1: usize, h2: usize) -> bool {
        let mut hv = h1;
        let mut new_kmer_occured = self.bv.add(hv);
        for _ in 1..self.number_of_hash{
            hv = (hv + h2) % self.modulo;
            new_kmer_occured |= self.bv.add(hv);
        }
        new_kmer_occured
    }
    #[inline]
    fn insert_inner(&mut self, kmer: &[u8]) -> bool {
        let (h1, h2) = self.hash_value(&kmer[0..self.k]);
        self.fill_hash_values(h1, h2)
    }
    /// Return Some(true) if the input kmer is novel kmer,
    /// Return Some(false) if the input is already in, and
    /// Return None if the input is too short.
    #[inline]
    pub fn insert(&mut self, kmer: &[u8]) -> Option<bool> {
        if kmer.len() < self.k {
            None
        } else {
            Some(self.insert_inner(kmer))
        }
    }
    #[inline]
    fn next_hash_value_by_masking(&self, h1: usize, h2: usize, x: u8) -> (usize, usize) {
        let (m1, m2) = ((self.mapper1)(x), (self.mapper2)(x));
        let h1 = ((h1 << 2) & self.mask) + m1 as usize;
        let h2 = ((h2 << 2) & self.mask) + m2 as usize;
        (h1, h2)
    }
    #[inline]
    fn next_with_underflow(&self, h:usize, m:usize, l:usize)->usize{
        let subset = (l * self.modulo_4) % self.modulo;
        let whole = (h << 2) + m;
        if subset > whole {
            ((self.modulo - subset) + whole) % self.modulo
        }else {
            (whole - subset ) % self.modulo
        }
    }
    #[inline]
    fn next_hash_value_by_modulo(&self, h1: usize, h2: usize, x: u8, y: u8) -> (usize, usize) {
        let (m1, m2) = ((self.mapper1)(x) as usize, (self.mapper2)(x) as usize);
        let (l1, l2) = ((self.mapper1)(y) as usize, (self.mapper2)(y) as usize);
        (self.next_with_underflow(h1,m1,l1),
         self.next_with_underflow(h2,m2,l2))
    }
    #[inline]
    fn next_hash_value(&self, h1: usize, h2: usize, x: u8, y: u8) -> (usize, usize) {
        let (h1, h2) = if self.mask < self.modulo {
            self.next_hash_value_by_masking(h1, h2, x)
        } else {
            self.next_hash_value_by_modulo(h1, h2, x, y)
        };
        (h1 % self.modulo, h2 % self.modulo)
    }
    #[inline]
    fn insert_string_inner(&mut self, input: &[u8]) -> bool {
        self.insert_string_pointing_inner(input).into_iter().any(|e|e)
    }
    #[inline]
    pub fn insert_string(&mut self, input: &[u8]) -> Option<bool> {
        if input.len() < self.k {
            None
        } else {
            Some(self.insert_string_inner(input))
        }
    }
    // Return the vector v, where v[i] == true <=>
    // input[i..i+k] is putative a new kmer into the bloom filter
    #[inline]
    fn insert_string_pointing_inner(&mut self, input: &[u8]) -> Vec<bool> {
        let (mut h1, mut h2) = self.hash_value(&input[0..self.k]);
        let mut new_kmer_vector = Vec::with_capacity(input.len() - self.k);
        for i in self.k..input.len() {
            new_kmer_vector.push(self.fill_hash_values(h1, h2));
            let (n1, n2) = self.next_hash_value(h1, h2, input[i], input[i - self.k]);
            h1 = n1;
            h2 = n2;
        }
        new_kmer_vector.push(self.fill_hash_values(h1, h2));
        new_kmer_vector
    }
    #[inline]
    pub fn insert_string_pointing(&mut self, input: &[u8]) -> Option<Vec<bool>> {
        if input.len() < self.k {
            None
        } else {
            Some(self.insert_string_pointing_inner(input))
        }
    }
    #[inline]
    fn insert_strings_inner(&mut self, input: &[Vec<u8>], k: usize) -> bool {
        let mut data_has_new_kmer = false;
        for line in input.iter().filter(|line| line.len() >= k) {
            data_has_new_kmer |= self.insert_string(line).unwrap();
        }
        data_has_new_kmer
    }
    pub fn insert_strings(&mut self, input: &[Vec<u8>]) -> Option<bool> {
        let k = self.k;
        if input.iter().all(|line| line.len() < k) {
            None
        } else {
            Some(self.insert_strings_inner(input, k))
        }
    }
    #[inline]
    fn has_inner(&self, kmer: &[u8]) -> bool {
        let (h1, h2) = self.hash_value(kmer);
        // eprintln!("{} -> h1:{}, h2:{}", String::from_utf8_lossy(kmer), h1, h2);
        (0..self.number_of_hash)
            .map(|i| (h1 + modulo_mul(h2, i, self.modulo)) % self.modulo)
            .all(|hv| self.bv.has(hv))
    }
    #[inline]
    pub fn has(&self, kmer: &[u8]) -> bool {
        kmer.len() == self.k && self.has_inner(kmer)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn mask_test() {
        let k = 2;
        let mask = create_masking_bits(k);
        assert_eq!(mask, 0b1111);
    }
    #[test]
    fn prime_check() {
        assert!(is_prime(2));
        assert!(is_prime(97));
        assert!(is_prime(541));
        assert!(!is_prime(10));
        assert!(!is_prime(33));
    }
    fn mock_mapper(_: u8) -> u8 {
        0
    }
    #[test]
    fn initialize_test() {
        let _bf = BloomFilter::new_with_default(12);
        assert!(BloomFilter::new_with_configures(173, 6, 12).is_some());
        assert!(
            BloomFilter::new_with_full_configures(173, 6, 12, mock_mapper, mock_mapper).is_some()
        );
        assert!(BloomFilter::new_with_configures(171, 6, 12).is_none());
        assert!(
            BloomFilter::new_with_full_configures(171, 6, 12, mock_mapper, mock_mapper).is_none()
        );
    }
    #[test]
    fn insert_test() {
        let mut bf = BloomFilter::new_with_default(14);
        let kmer = b"CATCATCGTAGCTT";
        assert_eq!(bf.insert(kmer), Some(true));
        let kmer = b"AT";
        assert_eq!(bf.insert(kmer), None);
    }
    #[test]
    fn insert_string_test() {
        let mut bf = BloomFilter::new_with_default(14);
        let kmer = b"ATTTACATCAGTGTCAGTCATGTCAGTGTCAT";
        assert_eq!(bf.insert_string(kmer), Some(true));
        assert_eq!(bf.insert_string(kmer), Some(false));
        let kmer = b"ATTTACATCAGTGTCAGTCATGTCAGTGTGAT";
        assert_eq!(bf.insert_string(kmer), Some(true));
        let kmer = b"ATTTA";
        assert_eq!(bf.insert_string(kmer), None);
    }
    #[test]
    fn insert_string_test2() {
        let mut bf = BloomFilter::new_with_default(14);
        let kmer = b"CATGTCAGTGTCAT";
        assert_eq!(bf.insert_string(kmer), Some(true));
        assert_eq!(bf.insert_string(kmer), Some(false));
        let kmer = b"CATGTCAGTGTGAT";
        assert_eq!(bf.insert_string(kmer), Some(true));
        let kmer = b"ATTTA";
        assert_eq!(bf.insert_string(kmer), None);
    }

    #[test]
    fn insert_strings_test() {
        let mut bf = BloomFilter::new_with_default(14);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
        ];
        assert_eq!(bf.insert_strings(&kmers), Some(true));
        assert_eq!(bf.insert_strings(&kmers), Some(false));
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        assert_eq!(bf.insert_strings(&kmers), Some(true));
    }
    #[test]
    fn insert_oneshot() {
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        assert!(BloomFilter::new_with_dataset(173, 14, &kmers).is_some());
    }
    #[test]
    fn retain_test() {
        let mut bf = BloomFilter::new_with_default(4);
        let kmer = b"AATG";
        bf.insert(kmer);
        assert!(bf.has(kmer));
        let kmer = b"AATC";
        assert!(!bf.has(kmer));
    }
    #[test]
    fn retain_string_test() {
        let mut bf = BloomFilter::new_with_default(4);
        // eprintln!("{}", bf);
        let line = b"AATGC";
        // eprintln!("");
        bf.insert_string(line);
        // eprintln!("First");
        bf.insert_string(&line[..4]);
        //eprintln!("Last");
        bf.insert_string(&line[1..]);
        for kmer in line.windows(4) {
            assert!(bf.has(kmer));
        }
    }
    #[test]
    fn multiple_insertion_test() {
        let mut bf = BloomFilter::new_with_default(14);
        let kmer = b"CATCATCGTAGCTT";
        assert_eq!(bf.insert(kmer), Some(true));
        assert_eq!(bf.insert(kmer), Some(false));
        let kmer = b"ATTTACATCAGTGTCAGTCATGTCAGTGTCAT";
        assert_eq!(bf.insert(kmer), Some(true));
        let kmer = b"ATTT";
        assert_eq!(bf.insert(kmer), None);
    }
    #[test]
    fn multiple_retain_test() {
        let mut bf = BloomFilter::new_with_default(14);
        let kmer = b"CATCATCGTAGCTT";
        assert_eq!(bf.insert(kmer), Some(true));
        assert_eq!(bf.insert(kmer), Some(false));
        let kmer2 = b"CATCATCGTAGCTG";
        assert_eq!(bf.insert(kmer2), Some(true));
        assert!(bf.has(kmer));
        assert!(bf.has(kmer2));
    }
    #[test]
    fn retain_test_2_normal() {
        let mut bf = BloomFilter::new_with_default(14);
        // println!("{}", bf);
        let line = b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec();
        assert_eq!(bf.insert_string(&line), Some(true));
        //eprintln!("{}", String::from_utf8_lossy(&line));
        for kmer in line.windows(14) {
            //eprint!("\t>{} ... ", String::from_utf8_lossy(kmer));
            assert!(bf.has(kmer));
            //eprintln!("OK!");
        }
    }
    #[test]
    fn multiple_retain_test_2() {
        let mut bf = BloomFilter::new_with_default(14);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        for line in &kmers {
            assert_eq!(bf.insert_string(line), Some(true));
        }
        for line in &kmers {
            //eprintln!("{}", String::from_utf8_lossy(line));
            for kmer in line.windows(14) {
                //eprint!("\t>{} ... ", String::from_utf8_lossy(kmer));
                assert!(bf.has(kmer));
                //eprintln!("OK!");
            }
        }
    }
    #[test]
    fn modulo_test() {
        assert_eq!(modulo_k_2(14, 173), 2usize.pow(14) % 173); // should be 122
        assert_eq!(modulo_k_2(1890, 173), 130);
        assert_eq!(modulo_k_2(600, 15487019), 6805415);
    }
    #[test]
    fn modulo_mul_test(){
        let modulo = 173;
        assert_eq!(modulo_mul(21,32,modulo),(21*32) % modulo);
        assert_eq!(modulo_mul(1,33,modulo),(1*33) % modulo);
        assert_eq!(modulo_mul(211,35,modulo),(211*35) % modulo);
        assert_eq!(modulo_mul(211,350,modulo),(211*350) % modulo);
    }
    use test::Bencher;
    #[bench]
    fn has_speed(b:&mut Bencher){
        let mut bf = BloomFilter::new_with_default(14);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CAGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        bf.insert_strings(&kmers);
        let kmers:Vec<Vec<u8>> = kmers.into_iter()
            .flat_map(|e|e.windows(14).map(|kmer|kmer.to_vec()).collect::<Vec<_>>())
            .collect();
        b.iter(||{
            for kmer in &kmers{
                bf.has(kmer);
            }
        });
    }
    #[bench]
    fn insert_speed(b:&mut Bencher){
        let mut bf = BloomFilter::new_with_default(20);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CATTGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        let hash_values:Vec<(usize,usize)> = kmers
            .into_iter()
            .flat_map(|e|e.windows(20).map(|kmer| bf.hash_value(kmer)).collect::<Vec<_>>())
            .collect();
        b.iter(||{
            for &(h1,h2) in &hash_values{
                bf.fill_hash_values(h1,h2);
            }
        });
    }
    #[bench]
    fn hash_value_speed(b:&mut Bencher){
        let bf = BloomFilter::new_with_default(20);
        let kmers = vec![
            b"CATCGATGCTAGCTAGTCAGTCGATGCTAGTCATTAC".to_vec(),
            b"CAGTCAGTACAGTGTAGCATCGTGA".to_vec(),
            b"CATGTAGCTAGTCATCGATC".to_vec(),
            b"CAGTGTGTGTCACACAGTGTGTACC".to_vec(),
        ];
        let mut sum = 0;
        b.iter(||{
            for input in &kmers{
                let (mut h1, mut h2) = bf.hash_value(&input[0..20]);
                for i in 20..input.len() {
                    let (n1, n2) = bf.next_hash_value(h1, h2, input[i], input[i - 20]);
                    h1 = n1;
                    h2 = n2;
                }
                sum += h1;
                sum += h2;
            }
        });
    }
    #[bench]
    fn insert_string_speed_length10_000(b:&mut Bencher){
        let mut bf = BloomFilter::new_with_default(20);
        use rand::{thread_rng,seq::SliceRandom};
        let mut rng = thread_rng();
        let line:Vec<_> = (0..10_000).filter_map(|_| BASES.choose(&mut rng))
            .map(|&e|e)
            .collect();
        b.iter(||bf.insert_string(&line).unwrap());
    }
}
